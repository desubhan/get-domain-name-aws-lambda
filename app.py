import json
from urllib.parse import urlparse

def lambda_handler(event, context):
    
    uri = json.loads(event["body"])["uri"]
    parsed_uri = urlparse(uri)
    result = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    
    return {
    'statusCode': 200,
    'headers': {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Content-Type",
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
    },
    'body': json.dumps(result)
}
