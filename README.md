# get-domain-name-aws-lambda

###### API Link : https://e1rhvdan8f.execute-api.us-east-1.amazonaws.com/default/get-domain-name

###### Request Body Format

**Example :**
```
{ "uri" : "http://www.example.com/subhan"}
```

**Response :**
```
"www.example.com"
```